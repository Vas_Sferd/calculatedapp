#!/bin/python

# STD
from random import random

# Импорт средства конфигурации
from kivy.config import Config
from kivy.core.window import Window

# Импорт App
from kivy.app import App

# Импорт шаблонов
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout

# Импорт Виджетов
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.button import Button

# Графика
from kivy.graphics import Canvas
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle

# Свойства
from kivy.properties import *

# Конфигурация окна
Config.set('graphics', 'resizable', False)

#Window.size = (1080 / 4, 1920 / 4)
#Window.min_size = (1080 / 4, 1920 / 4)
Window.clearcolor = (0.12, 0.22, 0.24, 1)


# Класс программы
class CalculatedApp(App):
    def __init__(self, **kwargs):
        # Конструируем родителя
        super().__init__(**kwargs)

        # Регистры
        self.eax = '0'
        self.ebx = ''
        self.edx = ''
        self.ecx = ''
        self.operation = ''

        # Лейбл
        self.lbl: Label

    def build(self):
        # Деление окна приложения на Экран и клавиатуру
        root = BoxLayout(
            orientation='vertical',
            padding=8,
        )

        # Экран (Label)
        self.lbl = Label(
            text='0',
            font_size='40sp',
            halign='right',
            valign='center',
            size_hint=(1, .25),
        )

        # Привязка расположения
        self.lbl.bind(size=self.lbl.setter('text_size'))

        # Добавление lbl в корневой виджет
        root.add_widget(self.lbl)

        # Клавиатура
        gl = GridLayout(
            cols=4,
            size_hint=(1, .6),
            spacing=2,
        )

        # Текст кнопок
        b_text = (
            '10^x',
            '√',
            'x²',
            '1/x',
            'CE',
            'C',
            '<X',
            '÷',
            '7',
            '8',
            '9',
            'X',
            '4',
            '5',
            '6',
            '-',
            '1',
            '2',
            '3',
            '+',
            '±',
            '0',
            ',',
            '='
        )

        # Генерация клавиатуры
        for i in b_text:
            new_button = Button(
                text=i,
                font_size='20sp',
                on_press=self.calc_o,
                color=(0.82, 0.86, 0.92, 1),
                background_color=(0.78, 0.62, 0.88, 1),
            )

            gl.add_widget(new_button)

        # Вкладывание Клавиатуры в корень
        root.add_widget(gl)

        # Возврат корневого узла
        return root

    # Функция, считывающая нажатия на кнопки
    def calc_o(self, instance):
        # Ввод числа или знака дробной части
        if instance.text in '0123456789,' and self.current() != 'Infinity':
            self.input(instance.text)

        # Операции ожидающие двух заполненных регистров
        elif instance.text in 'X÷-+':
            self.push_operation(instance.text)

        # Операции сразу выполняющися
        elif instance.text == '10^x'\
                or instance.text == '±'\
                or instance.text == '√'\
                or instance.text == 'x²'\
                or instance.text == '1/x':
            self.transform_current(instance.text)

        # Подсчёт результата
        elif instance.text == '=':
            self.result_calc()

        # Стирание группы символов
        elif instance.text in 'CE':
            self.set_current('0')
            if instance.text == 'C':
                self.clean_memory()

        # Стирание одного символа ( <X )
        elif instance.text == '<X':
            if len(self.current()) >= 2 and 'Infinity' not in self.current():
                self.set_current(self.current()[:-1])
            elif self.current() in [self.ecx, self.edx] and self.current() == '0':
                self.set_current('')
            elif self.current() == self.ebx and self.current() in ['', '0']:
                self.set_current('')
                self.operation = ''
            else:
                self.set_current('0')

        self.update_label()

    # Имя текущего регистра
    def current_name(self) -> str:
        if self.edx != '':
            return 'edx'
        elif self.operation != '':
            return 'ebx'
        elif self.ecx != '':
            return 'ecx'
        else:
            return 'eax'

    # Текущий регистр, доступный для заполнения
    def current(self) -> str:
        if self.edx != '':
            return self.edx
        elif self.operation != '':
            return self.ebx
        elif self.ecx != '':
            return self.ecx
        else:
            return self.eax

    # Редактирование текущего регистра
    def set_current(self, reg_value: str):
        if self.edx != '':
            self.edx = reg_value
        elif self.operation != '':
            self.ebx = reg_value
        elif self.ecx != '':
            self.ecx = reg_value
        else:
            self.eax = reg_value

    # Обновление вывода
    def update_label(self):
        # Выводим первое число
        self.lbl.text = self.eax
        # Ввод степени 10-ки для первого числа
        if self.ecx != '':
            self.lbl.text += ' X 10^(' + self.ecx + ')'

        # Проверка на наличие оператора
        if self.operation != '':
            self.lbl.text += ' ' + self.operation

        # Проверка на наличие непустого второго числа
        if self.ebx != '':
            self.lbl.text += ' ' + self.ebx
        # Ввод степени 10-ки для первого числа
        if self.edx != '':
            self.lbl.text += ' X 10^(' + self.edx + ')'

    # Выбираем или заменяем операцию или считаем предыдущую, если уже есть два числа
    def push_operation(self, operation: str):
        # Выполним предыдущую операцию, если второе число не пустое
        if self.ebx != '':
            self.result_calc()
        self.operation = operation

    # Вводим числовые символы
    def input(self, sym: str):
        # Устраняем ошибку
        if self.current() == 'E':
            self.set_current('0')

        # Встраиваем знак дробной части
        elif sym == ',':
            if ',' not in self.current() and self.current_name() not in ['ecx', 'edx']:
                self.set_current(self.current() + ',')

        # Добавляем новую цифру, всместо начального нуля
        elif self.current() == '0':
            self.set_current(sym)
        else:
            self.set_current(self.current() + sym)

    # Операции трансформирующие текущее число
    def transform_current(self, one_arg_operation: str):
        # Смена знака числа
        if one_arg_operation == '±' and self.current() != '' and self.current() != '0':
            if self.current_name() in ['eax', 'ecx', 'edx']:
                if self.current().startswith('-'):
                    self.set_current(self.current()[1:])
                else:
                    self.set_current('-' + self.current())
            elif self.operation == '+':
                self.operation = '-'
            elif self.operation == '-':
                self.operation = '+'
            else:
                self.ebx = '-' + self.ebx

        # Проверка на возможность выполнить действие
        elif self.current_name() not in ['ecx', 'edx'] and self.current() != '':
            # Начинаем ввод множителя 10-ки в степени
            if one_arg_operation == '10^x':
                if self.current_name() == 'eax' or self.ebx == '':
                    self.ecx = '0'  # Теперь current
                else:
                    self.edx = '0'  # Теперь current

            # Корень
            elif one_arg_operation == '√':
                if self.current() not in ['E', 'Infinity']:
                    if float(self.current()) > 0:
                        self.set_current(str(float(self.current()) ** 0.5))

            # Квадрат
            elif one_arg_operation == 'x²':
                if self.current() not in ['E', 'Infinity']:
                    self.set_current(str(float(self.current()) ** 2))

            # Обратное число
            elif one_arg_operation == '1/x':
                if self.current() not in ['E', 'Infinity', '0']:
                    self.set_current(str(1 / float(self.current())))

            # Убираем лишний ноль после точки
            if self.current().endswith('.0'):
                self.set_current(self.current()[:-2])

    # Считаем и обновляем результаты
    def result_calc(self):
        if self.ebx != '' and 'Infinity' not in self.eax:
            if self.ecx == '':
                self.ecx = '0'

            # num_a - первое число
            if int(self.ecx) + len(self.eax) > 20:
                num_a = 'Infinity'
            elif int(self.ecx) < -20:
                num_a = 0
            else:
                num_a = float(self.eax.replace(',', '.')) * 10 ** int(self.ecx)

            if self.edx == '':
                self.edx = '0'

            # num_b - второе число
            if int(self.edx) + len(self.ebx) > 20:
                num_b = 'Infinity'
            elif int(self.edx) < -20:
                num_b = 0
            else:
                num_b = float(self.ebx.replace(',', '.')) * 10 ** int(self.edx)

            operation = self.operation
            if 'Infinity' not in [str(num_a), str(num_b)]:
                if operation == '+':
                    result = str(num_a + num_b)
                elif operation == '-':
                    result = str(num_a - num_b)
                elif operation == 'X':
                    result = str(num_a * num_b)
                elif operation == '÷' and num_b != 0:
                    result = str(num_a / num_b)
                else:
                    # Ошибка
                    result = 'E'

                if 'e' in result and not result.startswith('-'):
                    result = 'Infinity'
                elif 'e' in result:
                    result = '-Infinity'
                elif result.endswith('.0'):
                    result = result[:-2]
            else:
                if operation == '+':
                    if self.eax.startswith('-') and int(self.ecx) > int(self.edx):
                        result = '-Infinity'
                    else:
                        result = 'Infinity'
                elif operation == '-':
                    if int(self.ecx) < int(self.edx) or self.eax.startswith('-'):
                        result = '-Infinity'
                    else:
                        result = 'Infinity'
                elif operation == 'X':
                    if self.eax.startswith('-') ^ self.ebx.startswith('-'):
                        result = '-Infinity'
                    else:
                        result = 'Infinity'
                elif operation == '÷' and num_b != 0:
                    if int(self.ecx) < int(self.edx):
                        result = '0'
                    else:
                        if self.eax.startswith('-') ^ self.ebx.startswith('-'):
                            result = '-Infinity'
                        else:
                            result = 'Infinity'
                else:
                    # Ошибка
                    result = 'E'

            self.clean_memory()
            self.eax = result
        # Обрабатываем Infinity в первом числе
        elif 'Infinity' in self.eax:
            result = self.eax
            self.clean_memory()
            self.eax = result
        # Применяем степень числа
        elif self.ecx != '':
            if int(self.ecx) + len(self.eax) > 20:
                result = 'Infinity'
            elif int(self.ecx) < -20:
                result = 0
            else:
                result = str(float(self.eax) * 10 ** int(self.ecx))

            if 'e' in str(result) and not self.eax.startswith('-'):
                result = 'Infinity'
            elif 'e' in str(result):
                result = '-Infinity'
            elif result.endswith('.0'):
                result = result[:-2]

            self.clean_memory()
            self.eax = result
        # Сбрасываем текущую операцию
        else:
            self.operation = ''

    def clean_memory(self):
        self.eax = '0'          # Первое число
        self.ebx = ''           # Второе число
        self.ecx = ''           # Степень множителя 10-ки для первого числа
        self.edx = ''           # Степень множителя 10-ки для второго числа
        self.operation = ''     # Знак операции


if __name__ == '__main__':
    CalculatedApp().run()
